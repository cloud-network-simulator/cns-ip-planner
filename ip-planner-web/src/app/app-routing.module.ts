import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddressDetailComponent} from './address-detail/address-detail.component';
import {InputAddressComponent} from './input-address/input-address.component';

const routes: Routes = [
  { path: 'detail/:id', component: AddressDetailComponent },
  { path: '', component: InputAddressComponent },
];

@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
  ],
})
export class AppRoutingModule { }
