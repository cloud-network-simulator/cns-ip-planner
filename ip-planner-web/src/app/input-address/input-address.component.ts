import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {IAddress} from '../address';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    public isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'app-input-address',
    styleUrls: ['./input-address.component.css'],
    templateUrl: './input-address.component.html',
})

export class InputAddressComponent implements OnInit {
    // public matcher = new MyErrorStateMatcher();
    public addresses: Array<IAddress> = [];

    // public addressFormControl = new FormControl('', [
    //   Validators.required,
    //   Validators.pattern('^([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}\\/\\d+$'),
    // ]);

    constructor(private http: HttpClient, public dialog: MatDialog) {
    }

    public ngOnInit(): void {
        this.get();
    }
    /**
     * method for create address dialog
     */
    public openDialog(): void {
        const dialogRef = this.dialog.open(CreateAddressDialog, {
            width: '250px',
        });

        dialogRef.afterClosed().subscribe(() => {
            console.log('The dialog was closed');
            this.get();
        });
    }
    /**
     * method for update address dialog
     */
    public openUpdateDialog(upAddr: IAddress): void {
        const dialogRef = this.dialog.open(UpdateAddressDialog, {
            data: { updateAddress: upAddr},
            width: '300px 500px',
        });

        dialogRef.afterClosed().subscribe(() => {
            console.log('The dialog update was closed');
            this.get();
        });
    }
    /**
     * fetch addresses from server database
     */
    public get(): void {
        this.http.get<Array<IAddress>>('http://localhost:8080/getAddresses')
            .subscribe((address) => this.addresses = address);
    }

}

/**
 * Class for dialog window to CREATE a new ip address
 */

@Component({
    selector: 'app-create-address-dialog',
    templateUrl: './create-address-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class CreateAddressDialog {
    public matcher = new MyErrorStateMatcher();

    constructor(
        public dialogRef: MatDialogRef<CreateAddressDialog>,
        private http: HttpClient) {
    }

    public addressFormControl = new FormControl('', [
        Validators.required,
        Validators.pattern('^([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}\\/\\d+$'),
    ]);

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public add(address: string): void {
        this.http.post('http://localhost:8080/inputAddress?address=' + address, null)
            .subscribe(
                (data) => {
                    alert('Your address has been added');
                    this.onNoClick();
                },
                (error) => {
                    if (error.status !== 200) {
                        alert('ERROR!' + error.status);
                    } else {
                        alert('Your address has been added');
                    }
                },
            );
    }
}

/**
 * dialog window to UPDATE address
 */
@Component({
    selector: 'app-update-address-dialog',
    templateUrl: './update-address-dialog.html',
})
// tslint:disable-next-line:component-class-suffix
export class UpdateAddressDialog {
    public matcher = new MyErrorStateMatcher();
    public inputAddress: string;

    constructor(
        public dialogRef: MatDialogRef<UpdateAddressDialog>,
        private http: HttpClient,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    public addressFormControl = new FormControl('', [
        Validators.required,
        Validators.pattern('^([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}\\/\\d+$'),
    ]);

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public onInputAddressChange() {

    }

    public delete(address: string): void {
        this.http.post('http://localhost:8080/delete?address=' + address, null)
            .subscribe(
                (data) => {
                    alert('The address was deleted');
                    this.onNoClick();
                },
                (error) => {
                    if (error.status !== 200) {
                        alert('ERROR!' + error.status);
                    } else {
                        alert('The address was deleted');
                        this.onNoClick();
                    }
                },
            );
    }

    public update(oldStringAddress: string, newStringAddress: string): void {
        this.http.post('http://localhost:8080/update?addressUpdate=' + oldStringAddress + '&newAddress=' + newStringAddress,
            null)
            .subscribe(
                (data) => {
                    alert('Your address was updated');
                    this.onNoClick();
                },
                (error) => {
                    if (error.status !== 200) {
                        alert('ERROR!' + error.status);
                    } else {
                        alert('Your address was modified');
                        this.onNoClick();
                    }
                },
            );
    }
}
