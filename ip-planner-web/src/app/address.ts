export class IAddress {
  public ipAddress: string;
  public mask: number;
  public id?: number;
}
