import { Location } from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IAddress} from '../address';

@Component({
  selector: 'app-address-detail',
  templateUrl: './address-detail.component.html',
  styleUrls: ['./address-detail.component.css'],
})
export class AddressDetailComponent implements OnInit {
  public addressDetail: IAddress;
  public oldStringAddress: string;
  public newStringAddress: string;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
  ) { }
  public ngOnInit(): void {
    this.getAddressById();
  }
  public getAddressById(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log('ID == ' + id);
    this.http.get<IAddress>('http://localhost:8080//getAddressById?id=' + id)
      .subscribe(
        (address) => {
          this.addressDetail = address;
          this.oldStringAddress = this.addressDetail.ipAddress + '/' + this.addressDetail.mask;
        },
      );
  }
  // экранировать /

  public goBack(): void {
    this.location.back();
  }
  public update(): void {
    this.http.post('http://localhost:8080/update?addressUpdate=' + this.oldStringAddress + '&newAddress=' + this.newStringAddress,
      null)
      .subscribe(
        (data) => { alert('Your address was updated'); this.getAddressById(); },
        (error) => {
          if (error.status !== 200) {
            alert('ERROR!' + error.status);
          } else {
            this.getAddressById();
            alert('Your address was modified');
          }
        },
      );
  }

}
