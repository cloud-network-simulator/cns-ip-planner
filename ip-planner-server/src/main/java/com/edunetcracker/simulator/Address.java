package com.edunetcracker.simulator;

import com.edunetcracker.simulator.exceptions.InternalServerErrorException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;
import java.util.regex.Pattern;

@Getter
@Setter
public class Address {
    private static final String regexpIP =
            "^([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}\\/\\d+$";
    private  static final String errorMessage = "Enter a valid IPv4 address";

    private final String address;
    private final String id;

    public Address(String address) {
        if(!Pattern.matches(regexpIP, address)){
            throw new InternalServerErrorException(errorMessage);
        }
        this.address = address;
        this.id = UUID.randomUUID().toString();
    }

}
