package com.edunetcracker.simulator.services;

import com.edunetcracker.simulator.database.repository.InputAddressRepository;
import com.edunetcracker.simulator.model.InputAddress;
import com.edunetcracker.simulator.services.status.InputAddressStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class InputAddressService {
    private static Logger logger = LoggerFactory.getLogger(InputAddressService.class);
    private InputAddressRepository inputAddressRepository;

    public InputAddressService(InputAddressRepository inputAddressRepository) {
        this.inputAddressRepository = inputAddressRepository;
    }

    public InputAddressStatus add(String address) {

        if (null == address) {
            return InputAddressStatus.NULL_ADDRESS;
        }

        InputAddress ia = stringToInputAddress(address);
        if (inputAddressRepository.findByIpAddressAndMask(ia.getIpAddress(), ia.getMask()) != null) {
            return InputAddressStatus.ADDRESS_ALREADY_EXISTS;
        }
        inputAddressRepository.save(ia);
        return InputAddressStatus.OK;
    }

    /**
     * DELETE THE IP ADDRESS FROM DB
     * @param address
     * @return
     */
    public InputAddressStatus delete(String address) {
        if (null == address) {
            return InputAddressStatus.NULL_ADDRESS;
        }
        InputAddress ia = stringToInputAddress(address);
        if (null == inputAddressRepository.findByIpAddressAndMask(ia.getIpAddress(), ia.getMask())) {
            return InputAddressStatus.DOES_NOT_EXIST;
        }
        inputAddressRepository.delete(inputAddressRepository.findByIpAddressAndMask(ia.getIpAddress(), ia.getMask()));
        return InputAddressStatus.OK;
    }

    /**
     * UPDATE ENTIRY IN DATABASE
     * @param addressToUpdate
     * @param update
     * @return status
     */
    public InputAddressStatus update(String addressToUpdate, String update) {
        if (null == addressToUpdate) {
            return InputAddressStatus.NULL_ADDRESS;
        }

        InputAddress ia = stringToInputAddress(addressToUpdate);
        InputAddress iaNew = stringToInputAddress(update);

        if (null == inputAddressRepository.findByIpAddressAndMask(ia.getIpAddress(), ia.getMask())) {
            return InputAddressStatus.DOES_NOT_EXIST;
        }
        InputAddress iaToUpdate = inputAddressRepository.findByIpAddressAndMask(ia.getIpAddress(), ia.getMask());
        iaToUpdate.setIpAddress(iaNew.getIpAddress());
        iaToUpdate.setMask(iaNew.getMask());
        inputAddressRepository.save(iaToUpdate);
        return InputAddressStatus.OK;
    }

    public InputAddress getAddressById(Integer id) {
        return inputAddressRepository.findById(id);
    }

    public List<InputAddress> getAll() {
        return inputAddressRepository.findAll();
    }

    /**
     * method to convert user input string to INPUT ADDRESS
     * @param address
     * @return InputAddress
     */
    public InputAddress stringToInputAddress (String address) {
        List<String> addrMask = new ArrayList<>(Arrays.asList(address.split("/")));
        return new InputAddress(addrMask.get(0), Integer.parseInt(addrMask.get(1)));
    }
}
