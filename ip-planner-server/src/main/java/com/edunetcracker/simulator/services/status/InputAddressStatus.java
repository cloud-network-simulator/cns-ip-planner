package com.edunetcracker.simulator.services.status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public enum InputAddressStatus {
    OK("", HttpStatus.OK),
    ADDRESS_ALREADY_EXISTS("Ip address {} was already added", HttpStatus.INTERNAL_SERVER_ERROR),
    NULL_ADDRESS("The ip address {} is null", HttpStatus.INTERNAL_SERVER_ERROR),
    DOES_NOT_EXIST("Ip address {} is not in database", HttpStatus.INTERNAL_SERVER_ERROR)
    ;

    private static Logger logger = LoggerFactory.getLogger(InputAddressStatus.class);
    private String body;
    private HttpStatus status;

    InputAddressStatus(String body, HttpStatus status) {
        this.body = body;
        this.status = status;
    }

    public void logError (Object ... params) {
        logger.error(body, params);
    }

    public void logWarning (Object ... params) {
        logger.warn(body, params);
    }

    public void logInfo (Object ... params) {
        logger.info(body, params);
    }

    public HttpStatus getHttpStatus () {
        return status;
    }

    public String getHttpBody () {
        return this.body;
    }

    public String getStringBody (Object ... params) {
        return String.format(body, params);
    }
}
