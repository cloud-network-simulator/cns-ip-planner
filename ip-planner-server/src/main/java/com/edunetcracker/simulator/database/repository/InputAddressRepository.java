package com.edunetcracker.simulator.database.repository;

import com.edunetcracker.simulator.model.InputAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InputAddressRepository extends JpaRepository<InputAddress, Long> {
    List<InputAddress> findByIpAddress(String ipAddress);
    InputAddress findById(Integer id);
    InputAddress findByIpAddressAndMask(String ipAddress, Integer mask);
//    @Modifying
//    @Query("update InputAddress u set u.ipAddress = ?1, u.mask = ?2 where u.id = ?3")
//    void updateInputAddressInfoById(String address, Integer mask, Integer id);
}
