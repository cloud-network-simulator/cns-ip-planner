package com.edunetcracker.simulator;

import com.edunetcracker.simulator.database.repository.InputAddressRepository;
import com.edunetcracker.simulator.model.InputAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

//TODO: Грамотное управление IP диапазонами
//Поддержка запроса на выделение подсети из указанного диапазона на необходимое количество хостов
//Помощь автоматического ассайна адресов
//Валидация подсети, настроенной вручную
//Поддержка приватных (серых) диапазонов
//Статистика использования плана сети (сколько адресов использовано из общего)
//1) Range adding
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
//@Component
//class Test implements CommandLineRunner {
//    @Autowired
//    InputAddressRepository iar;
//
//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println("hello it's my" );
//        iar.saveAndFlush(new InputAddress(1L, "hello"));
//    }
//}
