package com.edunetcracker.simulator.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class InputAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String ipAddress;
    private Integer mask;

    public InputAddress() {
        this.ipAddress = "";
        this.mask = null;
    }
    public InputAddress(String address, Integer mask) {
        this.ipAddress = address;
        this.mask = mask;
    }
}
