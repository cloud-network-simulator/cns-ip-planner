package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.Address;
import com.edunetcracker.simulator.model.InputAddress;
import com.edunetcracker.simulator.services.status.InputAddressStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import com.edunetcracker.simulator.services.InputAddressService;

import javax.xml.ws.Response;
import java.util.List;

@RestController
@Slf4j
public class InputAddressRestController {

    @Autowired
    private InputAddressService inputAddressService;

    @RequestMapping(value = "/inputAddress", method = RequestMethod.POST)
    public ResponseEntity inputAddress(@RequestParam String address) {
        InputAddressStatus ias = inputAddressService.add(address);
        if (ias.getHttpStatus() != HttpStatus.OK)
            return ResponseEntity.badRequest().body(ias.getStringBody());
        return ResponseEntity.ok().body(ias.getStringBody());
    }

    @RequestMapping(value = "/getAddresses", method = RequestMethod.GET)
    public List<InputAddress> getAddresses() {
        return inputAddressService.getAll();
    }

    @RequestMapping(value = "/getAddressById", method = RequestMethod.GET)
    public InputAddress getAddressById(String id) {
        return inputAddressService.getAddressById(Integer.parseInt(id));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@RequestParam String address) {
        InputAddressStatus ias = inputAddressService.delete(address);
        if (ias.getHttpStatus() != HttpStatus.OK)
            return ResponseEntity.badRequest().body(ias.getStringBody());
        return ResponseEntity.ok().body(ias.getStringBody());
    }
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity delete(@RequestParam String addressUpdate, @RequestParam String newAddress) {
        InputAddressStatus ias = inputAddressService.update(addressUpdate, newAddress);
        //TODO: check if new address already exists
        if (ias.getHttpStatus() != HttpStatus.OK)
            return ResponseEntity.badRequest().body(ias.getStringBody());
        return ResponseEntity.ok().body(ias.getStringBody());
    }
//
//    public List<Address> convertInputAddress(List<InputAddress> iadr) {
//        List<Address> result = new ArrayList<>();
//        for(InputAddress a: iadr) {
//            result.add(new Address(a.getIpAddress() + '/' + a.getMask()));
//        }
//        return result;
//    }

}

